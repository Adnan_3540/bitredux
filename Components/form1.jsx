import React, { Component } from "react";
import axios from "axios";
class AddCustomer extends Component {
  state = {
    employee: {
      id: "",
      name: "",
      section: "",
      math: 0,
      english: 0,
      computer: 0,
    },

    sports1: ["A", "B", "C", "D"],
    errors: {},
    view: 0,
  };

  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.validation();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;

    if (errCount > 0) return;
    console.log(this.state.employee.id);
    console.log(this.state.employee);

    try {
      axios
        .post(
          "https://localhost:2410/student",
          this.state.employee
        )
        .then((Response) => {
          console.log(Response);
          alert("Student added Successfully......");
          window.location = "/employee";
        })
        .catch((error) => {
          alert("Student not Add ......", error);
        });
    } catch (ex) {
      if (ex.Response && ex.Response.status === 400) {
        const errors = { ...this.state.errors };
        errors.username = ex.Response.data;
        this.setState({ errors });
      }
    }
  };
  validation = () => {
    let errs = {};
    if (!this.state.employee.name.trim()) errs.name = "Name is Required";
    else if (this.state.employee.name.trim() < 6)
      errs.name = "Salary is must be 6 Char.";
    if (!this.state.employee.id.trim()) errs.id = "ID is Required";
    else if (this.state.employee.id.trim().length < 4)
      errs.id = "Id is must be 4 Char.";

    if (!this.state.employee.section.trim())
      errs.section = "Section is Required";
    else if (this.state.employee.section === "Choose Section")
      errs.section = "You  Choose Section";
    if (this.state.employee.math < 0) errs.math = "Math is must Greater then.";
    else if (this.state.employee.math > 101)
      errs.math = "Math is must Greater then.";
    if (this.state.employee.computer < 0)
      errs.computer = "Coumputer is must Greater then.";
    else if (this.state.employee.computer > 101)
      errs.computer = "Computer is must Greater then.";
    if (this.state.employee.english < 0)
      errs.english = "English is must Greater then.";
    else if (this.state.employee.english > 101)
      errs.english = "English is must Greater then.";
    return errs;
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    const emp = { ...this.state.employee };
    input.type === "checkbox"
      ? (emp[input.name] = input.checked)
      : (emp[input.name] = input.value);
    this.setState({ employee: emp });
  };

  render() {
    const { employee } = this.state;

    return (
      <div className="container ">
        <h1>New Employee</h1>

        <form onSubmit={this.handleSubmit}>
          <div className="form-group ">
            <lable className="col" htmlFor="id">
              ID<h7 className="text-danger">*</h7>
            </lable>

            <input
              value={employee.id}
              onChange={this.handleChange}
              type="text"
              id="id"
              name="id"
              placeholder="Enter the User id"
              className="form-control"
            />
            <div className="alert text-danger">{this.state.errors.id}</div>
          </div>
          <div className="form-group ">
            <lable className="col" htmlFor="name">
              Name<h7 className="text-danger">*</h7>
            </lable>

            <input
              value={employee.name}
              onChange={this.handleChange}
              type="text"
              id="name"
              name="name"
              placeholder="Enter the User name"
              className="form-control"
            />
            <div className="alert text-danger">{this.state.errors.name}</div>
          </div>

          <div className="form-group ">
            <lable className="col" htmlFor="section">
              Section<h7 className="text-danger">*</h7>
            </lable>
            <select
              value={employee.section}
              onChange={this.handleChange}
              id="section"
              name="section"
              className="form-control"
            >
              <option>Choose Section</option>
              {this.state.sports1.map((c) => (
                <option>{c}</option>
              ))}
            </select>
            <div className="alert text-danger">{this.state.errors.section}</div>
          </div>

          <div className="form-group ">
            <lable className="" htmlFor="math">
              Math<h7 className="text-danger">*</h7>
            </lable>
            <input
              value={employee.math}
              onChange={this.handleChange}
              type="number"
              id="math"
              name="math"
              placeholder="Enter the Math Marks"
              className="form-control "
            />
            <div className="alert text-danger">{this.state.errors.math}</div>
          </div>

          <div className="form-group ">
            <lable className="" htmlFor="english">
              English<h7 className="text-danger">*</h7>
            </lable>
            <input
              value={employee.english}
              onChange={this.handleChange}
              type="english"
              id="english"
              name="english"
              placeholder="Enter the english Marks"
              className="form-control "
            />
            <div className="alert text-danger">{this.state.errors.english}</div>
          </div>
          <div className="form-group ">
            <lable className="" htmlFor="computer">
              Computer<h7 className="text-danger">*</h7>
            </lable>
            <input
              value={employee.computer}
              onChange={this.handleChange}
              type="number"
              id="computer"
              name="computer"
              placeholder="Enter the computer Marks"
              className="form-control "
            />
            <div className="alert text-danger">
              {this.state.errors.computer}
            </div>
          </div>
          <button className="btn btn-primary">ADD</button>
        </form>
      </div>
    );
  }
}
export default AddCustomer;
