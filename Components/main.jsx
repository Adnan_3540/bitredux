import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import Navbar from "./Navbar";
import "bootstrap/dist/css/bootstrap.css";
import Counter from "./counters";
import AddCustomer from "./form1";
import {createStore} from "redux";
import {Provider} from "react-redux";
const initialState={
  product:""
}
function reducer(state=initialState,action){
  switch(action.type){
    case "Q":
    return{
      ...state,
      product:action.val
    }
   
      default:return state;
  }
}
const store=createStore(reducer);
store.dispatch({type:"Q"});
class Main extends Component {
  render() {
    return (
      <div>
         <Provider store={store}>
         <Navbar />
        </Provider>
        <br />

        <div className="content">
          <Switch>
            <Route exact path="/" render={(props) => <Provider > <Counter  store={store} {...props} />  </Provider>} />
            <Route
              exact
              path="/student"
              render={(props) => <Provider store={store}> <Counter  store={store} {...props} />  </Provider>}
            />
            <Route
              exact
              path="/newStudent"
              render={(props) => <AddCustomer {...props} />}
            />
          </Switch>
        </div>
      </div>
    );
  }
}

export default Main;
