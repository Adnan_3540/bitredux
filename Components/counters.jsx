import React, { Component } from "react";
import axios from "axios";
import queryString from "query-string";
import CBArrayForm1 from "./formcb";
import {connect} from "react-redux";
class Counter extends Component {
  state = {
    detail: { department: "", designation: "", manager: "" },
    view: 0,
    E: "",
    class: "text-success",
    empId: "",
    employee: {
      id: "",
      name: "",
      section: "",
      math: 0,
      english: 0,
      computer: 0,
    },
    sports1: ["A", "B", "C", "D"],
    sports2: [" Excellent ", " Good ", " Average ", " Poor "],
    sports3: ["Excellent", "Good", "Average", "Poor"],
    sports4: ["Excellent ", "Good ", "Average ", "Poor "],
    errors: {},
    maxpage: "",
  };
 

  componentDidMount() {
    console.log( this.props.location.search)
    axios
      .get(
        "http://localhost:2410/student" +
          this.props.location.search
      )
      .then((Response) => {
        console.log(Response.data.length < 5);
        if (Response.data.length < 5) {
          this.setState({ user: Response.data, maxpage: "0" });
        } else {
          this.setState({ user: Response.data, maxpage: "" });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  componentDidUpdate(prevProps) {
    if (prevProps.location.search !== this.props.location.search) {
      axios
        .get(
          "http://localhost:2410/student" +
            this.props.location.search
        )
        .then((Response) => {
          if (Response.data.length < 5) {
            this.setState({ user: Response.data, maxpage: "0" });
          } else {
            this.setState({ user: Response.data, maxpage: "" });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }

  callURl = (params, sortBy, section, math, english, computer, page,product) => {
    let path = "/student";
    params = this.addToParams(params, "q", product);
    params = this.addToParams(params, "sortBy", sortBy);
    params = this.addToParams(params, "section", section);
    params = this.addToParams(params, "math", math);
    params = this.addToParams(params, "english", english);
    params = this.addToParams(params, "computer", computer);
    params = this.addToParams(params, "page", page);
    this.props.history.push({ pathname: path, search: params });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.validation();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    if (errCount > 0) return;
    // console.log(this.state.employee.id);
    // console.log(this.state.employee);

    try {
      axios
        .put(
          "http://localhost:2410/student/" +
            this.state.employee.name,
          this.state.employee
        )
        .then((Response) => {
          //console.log(Response);
          alert("student Edit Successfully......");
          window.location = "/student";
        })
        .catch((error) => {
          console.log(error);
        });
    } catch (ex) {
      if (ex.Response && ex.Response.status === 400) {
        const errors = { ...this.state.errors };
        errors.username = ex.Response.data;
        this.setState({ errors });
      }
    }
  };

  validation = () => {
    let errs = {};
    if (!this.state.employee.name.trim()) errs.name = "Name is Required";
    else if (this.state.employee.name.trim() < 6)
      errs.name = "Salary is must be 6 Char.";
    if (!this.state.employee.id.trim()) errs.id = "ID is Required";
    else if (this.state.employee.id.trim().length < 4)
      errs.id = "Id is must be 4 Char.";

    if (!this.state.employee.section.trim())
      errs.section = "Section is Required";
    else if (this.state.employee.section === "Choose Section")
      errs.section = "You  Choose Section";
    if (this.state.employee.math < 0) errs.math = "Math is must Greater then.";
    else if (this.state.employee.math > 101)
      errs.math = "Math is must Greater then.";
    if (this.state.employee.computer < 0)
      errs.computer = "Coumputer is must Greater then.";
    else if (this.state.employee.computer > 101)
      errs.computer = "Computer is must Greater then.";
    if (this.state.employee.english < 0)
      errs.english = "English is must Greater then.";
    else if (this.state.employee.english > 101)
      errs.english = "English is must Greater then.";
    return errs;
  };

  handleChange = (e) => {
    const { currentTarget: input } = e;

    const emp = { ...this.state.employee };
    input.type === "checkbox"
      ? (emp[input.name] = input.checked)
      : (emp[input.name] = input.value);

    this.setState({ employee: emp });
  };
  handleSort = (sort) => {
    let { section, math, english, computer, page,q } = queryString.parse(
      this.props.location.search
    );
    this.callURl("", sort, section, math, english, computer, page ,q);
  };
  handleDelete = (i) => {
    try {
      axios
        .delete("http://localhost:2410/student/" + i)
        .then((Response) => {
          //console.log(Response);
          window.location = "/student";
          alert("student Delete Successfully......");
        })
        .catch((error) => {
          console.log(error);
        });
    } catch (ex) {
      if (ex.Response && ex.Response.status === 400) {
        const errors = { ...this.state.errors };
        errors.username = ex.Response.data;
        this.setState({ errors });
      }
    }
  };

  handleEdit = (i) => {
    this.setState({
      view: 1,
      editIndex: i,
      employee: {
        id: this.state.user[i].id,
        name: this.state.user[i].name,
        section: this.state.user[i].section,
        math: this.state.user[i].math,
        english: this.state.user[i].english,
        computer: this.state.user[i].computer,
      },
    });
  };
  makeRadioStructure(sports, sport) {
    let sportsRadio = {
      sports: sports,
      select: sport,
    };

    return sportsRadio;
  }

  makeCbStructure(names, players) {
    let temp = names.map((n1) => ({
      name: n1,
      selected: false,
    }));
    //  console.log(players);
    if (players !== undefined) {
      let cnames = players.split(",");
      for (let i = 0; i < cnames.length; i++) {
        let obj = temp.find((n1) => n1.name === cnames[i]);
        if (obj) obj.selected = true;
      }
    }

    return temp;
  }

  handleOptionChange = (namesCheckbox) => {
    console.log(" handleOptionChange");
    let filteredNames = namesCheckbox.filter((n1) => n1.selected);
    let arrayNames = filteredNames.map((n1) => n1.name);
    let players = arrayNames.join(",");
    let { sortBy, math, english, computer,q } = queryString.parse(
      this.props.location.search
    );
    this.callURl("", sortBy, players, math, english, computer, 1 ,q);
  };
  handleOptionChange1 = (namesCheckbox) => {
    console.log(" handleOptionChange1");
    let filteredNames = namesCheckbox.filter((n1) => n1.selected);
    let arrayNames = filteredNames.map((n1) => n1.name);
    let players = arrayNames.join(",");
    let { sortBy, section, english, computer,q } = queryString.parse(
      this.props.location.search
    );
    this.callURl("", sortBy, section, players, english, computer, 1,q);
  };
  handleOptionChange21 = (namesCheckbox) => {
    console.log(" handleOptionChange21");
    let filteredNames = namesCheckbox.filter((n1) => n1.selected);
    let arrayNames = filteredNames.map((n1) => n1.name);
    let players = arrayNames.join(",");
    let { sortBy, section, math, computer,q } = queryString.parse(
      this.props.location.search
    );
    this.callURl("", sortBy, section, math, players, computer, 1,q);
  };
  handleOptionChange31 = (namesCheckbox) => {
    console.log(" handleOptionChange31");
    let filteredNames = namesCheckbox.filter((n1) => n1.selected);
    let arrayNames = filteredNames.map((n1) => n1.name);
    let players = arrayNames.join(",");
    let { sortBy, section, math, english ,q} = queryString.parse(
      this.props.location.search
    );
    this.callURl("", sortBy, section, math, english, players, 1,q);
  };

  goto = (x) => {
    let { page, sortBy, section, math, english, computer,q } = queryString.parse(
      this.props.location.search
    );
    let currPage = page ? +page : 1;
    currPage = currPage + x;
    this.callURl("", sortBy, section, math, english, computer, currPage,q);
  };
  render() {
    let { sortBy, section, math, english, computer, page,q } = queryString.parse(
      this.props.location.search
    );
    console.log(q)
    if (page === undefined) {
      page = 1;
      this.callURl("", sortBy, section, math, english, computer, page,);
    }
   
  
    let { sports1, sports2, sports3, sports4 } = this.state;
    let namesCheckbox = this.makeCbStructure(sports1, section);
    let namesCheckbox1 = this.makeCbStructure(sports2, math);
    let namesCheckbox2 = this.makeCbStructure(sports3, english);
    let namesCheckbox3 = this.makeCbStructure(sports4, computer);
    const { employee, maxpage } = this.state;
    console.log(this.props.product);
    return (
      <div className="container ">
        {this.state.view === 0 ? (
          <div>
            {this.state.user !== undefined ? (
              <div>
                <div className="row">
                  <div className="col-2 border text-center bg-light">
                    <div>
                      <h1 className="bg-light">Options</h1>
                      <hr />
                      <h3 className="bg-light">Section</h3>
                      <CBArrayForm1
                        namesCheckbox={namesCheckbox}
                        onOptionChange={this.handleOptionChange}
                      />
                      <hr />
                      <h3 className="bg-light">Math</h3>

                      <CBArrayForm1
                        namesCheckbox={namesCheckbox1}
                        onOptionChange={this.handleOptionChange1}
                      />
                      <hr />
                      <h3 className="bg-light">English</h3>
                      <CBArrayForm1
                        namesCheckbox={namesCheckbox2}
                        onOptionChange={this.handleOptionChange21}
                      />
                      <hr />
                      <h3 className="bg-light">Computer</h3>
                      <CBArrayForm1
                        namesCheckbox={namesCheckbox3}
                        onOptionChange={this.handleOptionChange31}
                      />
                      <hr />
                    </div>
                  </div>
                  <div className="col-10">
                    <div className="row">
                      <div
                        className="border col text-white bg-dark text-center"
                        onClick={() => this.handleSort("id")}
                      >
                        ID
                      </div>
                      <div
                        className="border col text-white bg-dark text-center"
                        onClick={() => this.handleSort("name")}
                      >
                        Name
                      </div>
                      <div
                        className="border col text-white bg-dark text-center"
                        onClick={() => this.handleSort("section")}
                      >
                        Section
                      </div>
                      <div
                        className="border col text-white bg-dark text-center"
                        onClick={() => this.handleSort("math")}
                      >
                        Math
                      </div>
                      <div
                        className="border col text-white bg-dark text-center"
                        onClick={() => this.handleSort("english")}
                      >
                        English
                      </div>
                      <div
                        className="border col text-white bg-dark text-center"
                        onClick={() => this.handleSort("computer")}
                      >
                        Computer
                      </div>

                      <div className="border col text-white bg-dark text-center">
                        Edit
                      </div>
                      <div className="border col text-white bg-dark text-center">
                        Delete
                      </div>
                    </div>
                    <div>
                      {this.state.user.map((st, index) => (
                        <div className=" border row">
                          <div className="col text-center">{st.id}</div>
                          <div className="col text-center">{st.name}</div>

                          <div className="col text-center">{st.section}</div>
                          <div className="col text-center">{st.math}</div>
                          <div className="col text-center">{st.english}</div>
                          <div className="col text-center">{st.computer}</div>
                          <div className="col text-center">
                            <div
                              className=""
                              onClick={() => this.handleEdit(index)}
                            >
                              <i class="fa fa-clipboard" aria-hidden="true"></i>
                            </div>
                          </div>
                          <div className=" col text-center">
                            <div
                              className=""
                              onClick={() => this.handleDelete(st.name)}
                            >
                              <i class="fa fa-trash" aria-hidden="false"></i>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>

                    <div className="row">
                      <div className="col-6 text-left">
                        {page > 1 ? (
                          <button
                            className="m-2 btn btn-primary"
                            onClick={() => this.goto(-1)}
                          >
                            Previous
                          </button>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="col-6 text-right">
                        {maxpage === "" ? (
                          <button
                            className="m-2 btn btn-primary"
                            onClick={() => this.goto(1)}
                          >
                            Next
                          </button>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        ) : (
          <div className="container ">
            <br />
            <h1>Edit Employee</h1>

            <form onSubmit={this.handleSubmit}>
              <div className="form-group ">
                <lable className="col" htmlFor="id">
                  ID<h7 className="text-danger">*</h7>
                </lable>

                <input
                  value={employee.id}
                  onChange={this.handleChange}
                  type="text"
                  id="id"
                  name="id"
                  placeholder="Enter the User id"
                  className="form-control"
                />
                <div className="alert text-danger">{this.state.errors.id}</div>
              </div>
              <div className="form-group ">
                <lable className="col" htmlFor="name">
                  Name<h7 className="text-danger">*</h7>
                </lable>

                <input
                  value={employee.name}
                  onChange={this.handleChange}
                  type="text"
                  id="name"
                  name="name"
                  placeholder="Enter the User name"
                  className="form-control"
                />
                <div className="alert text-danger">
                  {this.state.errors.name}
                </div>
              </div>

              <div className="form-group ">
                <lable className="col" htmlFor="section">
                  Section<h7 className="text-danger">*</h7>
                </lable>
                <select
                  value={employee.section}
                  onChange={this.handleChange}
                  id="section"
                  name="section"
                  className="form-control"
                >
                  <option>Choose Section</option>
                  {this.state.sports1.map((c) => (
                    <option>{c}</option>
                  ))}
                </select>
                <div className="alert text-danger">
                  {this.state.errors.section}
                </div>
              </div>

              <div className="form-group ">
                <lable className="" htmlFor="math">
                  Math<h7 className="text-danger">*</h7>
                </lable>
                <input
                  value={employee.math}
                  onChange={this.handleChange}
                  type="number"
                  id="math"
                  name="math"
                  placeholder="Enter the Math Marks"
                  className="form-control "
                />
                <div className="alert text-danger">
                  {this.state.errors.math}
                </div>
              </div>

              <div className="form-group ">
                <lable className="" htmlFor="english">
                  English<h7 className="text-danger">*</h7>
                </lable>
                <input
                  value={employee.english}
                  onChange={this.handleChange}
                  type="english"
                  id="english"
                  name="english"
                  placeholder="Enter the english Marks"
                  className="form-control "
                />
                <div className="alert text-danger">
                  {this.state.errors.english}
                </div>
              </div>
              <div className="form-group ">
                <lable className="" htmlFor="computer">
                  Computer<h7 className="text-danger">*</h7>
                </lable>
                <input
                  value={employee.computer}
                  onChange={this.handleChange}
                  type="number"
                  id="computer"
                  name="computer"
                  placeholder="Enter the computer Marks"
                  className="form-control "
                />
                <div className="alert text-danger">
                  {this.state.errors.computer}
                </div>
              </div>
              <button className="btn btn-primary">Edit</button>
              <br />
            </form>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps=state=>({
  product:state.product

})
export default connect(mapStateToProps) (Counter);
