import React, { Component } from "react";

class CBArrayForm extends Component {
  handleChange = (e) => {
    const { currentTarget: input } = e;
    const { namesCheckbox, sportsRadio } = this.props;
    if (input.type === "checkbox") {
      let cb = namesCheckbox.find((n1) => n1.name === input.name);
      if (cb) cb.selected = input.checked;
    } else if (input.name === "selectedSport") {
      sportsRadio[input.name] = input.value;
    }

    this.props.onOptionChange(sportsRadio);
  };
  render() {
    const { sportsRadio } = this.props;

    return (
      <div>
        <form>
          {sportsRadio.sports.map((item) => (
            <div className="form-check" key={item}>
              <input
                value={item}
                onChange={this.handleChange}
                id={item}
                type="radio"
                name="selectedSport"
                checked={item === sportsRadio.select}
                className="form-check-input"
              />
              <label className="form-check-label" htmlFor={item}>
                {item}
              </label>
            </div>
          ))}
        </form>
      </div>
    );
  }
}
export default CBArrayForm;
