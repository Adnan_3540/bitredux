import React, { Component } from "react";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import {connect} from "react-redux";

class Navbar extends Component {
  state = {  employee: { name: "" } };

  handleSubmit = (e) => {
    e.preventDefault();
  this.props.dispatch({type:"Q",val: this.state.employee.name});
}
  handleChange = (e) => {
    const { currentTarget: input } = e;
    const emp = { ...this.state.employee };
    input.type === "checkbox"
      ? (emp[input.name] = input.checked)
      : (emp[input.name] = input.value);

    this.setState({ employee: emp });
  };
  render() {
    let employee = this.state.employee;
    console.log(this.props.product);
    return (
      <div className="container bg-dark text-white">
        <div class="navbar navbar-expand-lg navbar-light bg-dark text-white">
          <Link
            class="nav-link  text-white"
            to="/"
            tabIndex="-1"
            aria-disabled="true"
          >
            <h1></h1>
          </Link>
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>

          <div
            class="collapse navbar-collapse text-white"
            id="navbarSupportedContent"
          >
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
              <li className="nav-item  text-white">
                <Link
                  className="nav-link  text-white"
                  to="/student"
                  tabIndex="-1"
                  aria-disabled="true"
                >
                  Show Student
                </Link>
              </li>
              <li className="nav-item  text-white">
                <Link
                  className="nav-link  text-white"
                  to="/newStudent"
                  tabIndex="-1"
                  aria-disabled="true"
                >
                  Add a New Student
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
              <li class="nav-item">
              <form onSubmit={this.handleSubmit} className="row">
              <input
                      value={employee.name}
                      onChange={this.handleChange}
                      onKeyPress={this.keyPressed}
                      type="text"
                      id="name"
                      name="name"
                      className="form-control mt-2 col-9"
                      placeholder="Search For Movies"
                      style={{height:"35px","font-style":"normal",width:"300px"}}
                    />
                    <button type="submit" className="col-3 mt-2 btn btn-light"  style={{height:"35px"}}>Search</button>
                    </form>
              </li>
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
             
             
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
              <li class="nav-item">
                <Link
                  class="nav-link  text-white"
                  tabIndex="-1"
                  aria-disabled="true"
                ></Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps=state=>({
  product:state.product

})
export default connect(mapStateToProps) (Navbar);
